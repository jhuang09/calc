git config --global user.name "Janny Huang"
git config --global user.email "janny@mit.edu"

#git remote rm origin
#git remote set-url --add origin https://github.com/jhuang09/calculator.git
#git remote set-url origin https://jhuang09:${GH_TOKEN}@gitlab.com/calculator.git
git remote set-url origin http://${USERNAME}:${GH_PAC}@github.com/${USERNAME}/calculator.git
rm -rf node_modules/*
npm install

npm run build

echo $CI_COMMIT_REF_NAME
distName="$CI_COMMIT_REF_NAME-distribution-gitlab"
BUILD_DIR=./build
echo $distName

rm -rf .tmp
rm -rf node_modules

# git push -u origin --delete $distName
git push -u origin --delete $distName
git checkout --quiet -b $distName

git remote -v

# remove the file that states to ignore the build folder
rm .gitignore 

git add --all .
git commit --quiet -m "[ci skip] Distribution Release"
echo "Dist Name: $distName "

# grab only the contents of the build folder
git filter-branch --prune-empty --subdirectory-filter $BUILD_DIR $distName

git filter-branch -f --msg-filter 'echo "[ci skip] Distribution Release" && cat'

#git push -u origin $distName
git push -u origin $distName

git checkout $CI_COMMIT_REF_NAME

