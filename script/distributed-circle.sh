git config --global user.name "Janny Huang"
git config --global user.email "janny@mit.edu"

rm -rf node_modules/*
npm install

npm run build

echo $CI_COMMIT_REF_NAME
distName="$CIRCLE_BRANCH-distribution-circle-rel"
BUILD_DIR=./build
echo $distName

rm -rf .tmp
rm -rf node_modules

git push origin --delete $distName
git checkout --quiet -b $distName

# remove the file that states to ignore the build folder
rm .gitignore

ls -ltr $BUILD_DIR
git add --all .
git commit --quiet -m "[ci skip] Distribution Release"
echo "Dist Name: $distName "

# grab only the contents of the build folder
git filter-branch --prune-empty --subdirectory-filter $BUILD_DIR $distName

git filter-branch -f --msg-filter 'echo "[ci skip] Distribution Release" && cat'

git push origin $distName

git checkout $CIRCLE_BRANCH

