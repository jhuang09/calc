# CircleCI script!
deploy=`cat ./script/version | cut -d',' -f1 `
version=`cat ./script/version | cut -d',' -f2`
project=`cat ./script/version | cut -d',' -f3`
BUILD_DIR=./build
GIT_REPO="git@github.com:jhuang09/calculator.git"
PROJECT=calculator

echo "Deploying File $a"
if [[ "$deploy" == "true" ]]
then

    # Update version file to be false
    git config --global user.email "janny@mit.edu"
    git config --global user.name "Janny"

    distName="$CIRCLE_BRANCH-distribution-circle-rel"

    tagVersion1=`cat ./script/version  | cut -d',' -f2 | cut -d'.' -f1`
    tagVersion2=`cat ./script/version  | cut -d',' -f2 | cut -d'.' -f2`
    tagVersion3=`cat ./script/version | cut -d',' -f2 | cut -d'.' -f3`

    echo $tagVersion3
    tag="$CIRCLE_BRANCH-circle-$tagVersion1.$tagVersion2.$tagVersion3"
    distTag="$CIRCLE_BRANCH-circle-dcistribution-tag-$tagVersion1.$tagVersion2.$tagVersion3"
    tagVersion="$tagVersion1.$tagVersion2.$tagVersion3"
    echo $tag
    echo $distTag

    ((tagVersion3+=1))
    echo $tagVersion3
    newTagVersion="$tagVersion1.$tagVersion2.$tagVersion3"
    newTag="$tagVersion1.$tagVersion2.$tagVersion3"
    echo $newTag

    git checkout $CIRCLE_BRANCH

    echo
    echo "*********  Update deployFile to false"
    echo "false,$tagVersion,$project" >./script/version 

    git add --all
    git commit -m "[ci skip] Updating deployFile to false - original version $tag"
    git push origin $CIRCLE_BRANCH

    # Tag current branch
    git add --all
    git commit -m "[ci skip] Tagging release"
    git tag $tag -a -m "[ci skip] Generated tag"
    git push --tags

    # npm
    ls -l node_modules
    rm -rf node_modules/*
    npm install
    pwd
    
    ls -a src

    npm run build
    
    if [ $? -ne 0 ]
    then
        echo
        
        echo
        echo "****** Build failed stopping *******"
        echo
        echo
        exit 1
    else

        rm -rf node
        rm -rf node_modules

        rm .gitignore

        # Delete the distribution branch
        git push origin --delete $distName

        # Create a fresh clean distribution branch
        #     this will pickup the built changes
        git checkout --quiet -b $distName
        
        ls -ltr $BUILD_DIR

        git add --all .
        git commit -m "[ci skip] Distribution Release"
    
        #Filter only the build subdirectory
        # push just the build to the distribution branch
        git filter-branch --prune-empty --subdirectory-filter $BUILD_DIR $distName 

        # disable build trigger
        git filter-branch -f --msg-filter 'echo "[ci skip] Distribution Tagged Version Release" && cat'
        
        # Push new disribution branch
        git push origin $distName

        #create a clean directory with the distribution to tag
        #  tag will tag everything, not just the dist dirctory
        pwd
        mkdir ../deploy
        cd ../deploy
        rm -rf *
        pwd
        git clone --branch=$distName $GIT_REPO
        cd $PROJECT

        ls
       #  #Step 5, tag distribution branch
        git tag $distTag -a -m "[ci skip] Testing Tagging distribution"
        git push --tags

        ls ../
        cd ../$PROJECT
        git checkout $CIRCLE_BRANCH

        echo
        echo "*********  Update deployFile to false with new Tag Version $newTagVersion"
        echo "false,$newTagVersion,$project" >./script/version

        git add --all
       git commit -m "Updating release to false - new version $newTag"
       git push origin $CIRCLE_BRANCH

    fi
fi
